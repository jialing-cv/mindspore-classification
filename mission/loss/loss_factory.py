# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""loss factory"""

import mindspore.nn as nn
from .cross_entropy_smooth import CrossEntropySmooth


def create_loss(args):
    if args.loss == 'cross_entropy_smooth':
        loss = CrossEntropySmooth(smooth_factor=args.smooth_factor,
                                  factor=args.factor)
    elif args.loss == 'softmax_cross_entropy_with_logits':
        loss = nn.SoftmaxCrossEntropyWithLogits(sparse=args.sparse,
                                                reduction=args.reduction)
    return loss
