from mindspore import nn

from .layers.conv_norm_act  import Conv2dNormActivation


class MobileNetV1(nn.Cell):
    """
    MobileNet V1 backbone.
    """

    def __init__(self):
        super(MobileNetV1, self).__init__()
        self.layers = [
            Conv2dNormActivation(3, 32, 3, 2, activation=nn.ReLU6),  # Conv0

            Conv2dNormActivation(32, 32, 3, 1, groups=32, activation=nn.ReLU6),  # Conv1_depthwise
            Conv2dNormActivation(32, 64, 1, 1, activation=nn.ReLU6),  # Conv1_pointwise
            Conv2dNormActivation(64, 64, 3, 2, groups=64, activation=nn.ReLU6),  # Conv2_depthwise
            Conv2dNormActivation(64, 128, 1, 1, activation=nn.ReLU6),  # Conv2_pointwise

            Conv2dNormActivation(128, 128, 3, 1, groups=128, activation=nn.ReLU6),  # Conv3_depthwise
            Conv2dNormActivation(128, 128, 1, 1, activation=nn.ReLU6),  # Conv3_pointwise
            Conv2dNormActivation(128, 128, 3, 2, groups=128, activation=nn.ReLU6),  # Conv4_depthwise
            Conv2dNormActivation(128, 256, 1, 1, activation=nn.ReLU6),  # Conv4_pointwise

            Conv2dNormActivation(256, 256, 3, 1, groups=256, activation=nn.ReLU6),  # Conv5_depthwise
            Conv2dNormActivation(256, 256, 1, 1, activation=nn.ReLU6),  # Conv5_pointwise
            Conv2dNormActivation(256, 256, 3, 2, groups=256, activation=nn.ReLU6),  # Conv6_depthwise
            Conv2dNormActivation(256, 512, 1, 1, activation=nn.ReLU6),  # Conv6_pointwise

            Conv2dNormActivation(512, 512, 3, 1, groups=512, activation=nn.ReLU6),  # Conv7_depthwise
            Conv2dNormActivation(512, 512, 1, 1, activation=nn.ReLU6),  # Conv7_pointwise
            Conv2dNormActivation(512, 512, 3, 1, groups=512, activation=nn.ReLU6),  # Conv8_depthwise
            Conv2dNormActivation(512, 512, 1, 1, activation=nn.ReLU6),  # Conv8_pointwise
            Conv2dNormActivation(512, 512, 3, 1, groups=512, activation=nn.ReLU6),  # Conv9_depthwise
            Conv2dNormActivation(512, 512, 1, 1, activation=nn.ReLU6),  # Conv9_pointwise
            Conv2dNormActivation(512, 512, 3, 1, groups=512, activation=nn.ReLU6),  # Conv10_depthwise
            Conv2dNormActivation(512, 512, 1, 1, activation=nn.ReLU6),  # Conv10_pointwise
            Conv2dNormActivation(512, 512, 3, 1, groups=512, activation=nn.ReLU6),  # Conv11_depthwise
            Conv2dNormActivation(512, 512, 1, 1, activation=nn.ReLU6),  # Conv11_pointwise

            Conv2dNormActivation(512, 512, 3, 2, groups=512, activation=nn.ReLU6),  # Conv12_depthwise
            Conv2dNormActivation(512, 1024, 1, 1, activation=nn.ReLU6),  # Conv12_pointwise
            Conv2dNormActivation(1024, 1024, 3, 1, groups=1024, activation=nn.ReLU6),  # Conv13_depthwise
            Conv2dNormActivation(1024, 1024, 1, 1, activation=nn.ReLU6),  # Conv13_pointwise
        ]

        self.features = nn.SequentialCell(self.layers)

    def construct(self, x):
        """Forward pass"""
        output = self.features(x)
        return output

