# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

"""
MindSpore implementation of `ShuffleNetV2`.
Refer to ShuffleNet V2: Practical Guidelines for Efficient CNN Architecture Design
"""

import mindspore.nn as nn
import mindspore.ops as ops
import mindspore.common.initializer as init

__all__ = [
    "ShuffleNetV2",
    "shufflenet_v2_x0_5",
    "shufflenet_v2_x1_0",
    "shufflenet_v2_x1_5",
    "shufflenet_v2_x2_0"
]


def _default_cfgs(url=''):
    return {
        'url': url, 'num_classes': 1000, 'input_size': (3, 224, 224), 'pool_size': (7, 7),
        'mean': (0.485, 0.456, 0.406), 'std': (0.229, 0.224, 0.225)
    }


model_cfgs = {
    'shufflenet_v2_x1_0': _default_cfgs(url='shufflenet_v2_x1_0.ckpt'),
}


class ShuffleV2Block(nn.Cell):
    """Basic block of ShuffleNetV2."""

    def __init__(self, inp, oup, mid_channels, ksize, stride):
        super(ShuffleV2Block, self).__init__()
        assert stride in [1, 2]
        self.stride = stride
        self.mid_channels = mid_channels
        self.ksize = ksize
        pad = ksize // 2
        self.pad = pad
        self.inp = inp

        outputs = oup - inp
        branch_main = [
            # pw
            nn.Conv2d(inp, mid_channels, 1, 1, pad_mode='pad', padding=0, has_bias=False),
            nn.BatchNorm2d(mid_channels),
            nn.ReLU(),
            # dw
            nn.Conv2d(mid_channels, mid_channels, ksize, stride,
                      pad_mode='pad', padding=pad, group=mid_channels, has_bias=False),
            nn.BatchNorm2d(mid_channels),
            # pw-linear
            nn.Conv2d(mid_channels, outputs, 1, 1, pad_mode='pad', padding=0, has_bias=False),
            nn.BatchNorm2d(outputs),
            nn.ReLU(),
        ]
        self.branch_main = nn.SequentialCell(branch_main)

        if stride == 2:
            branch_proj = [
                # dw
                nn.Conv2d(inp, inp, ksize, stride,
                          pad_mode='pad', padding=pad, group=inp, has_bias=False),
                nn.BatchNorm2d(inp),
                # pw-linear
                nn.Conv2d(inp, inp, 1, 1, pad_mode='pad', padding=0, has_bias=False),
                nn.BatchNorm2d(inp),
                nn.ReLU(),
            ]
            self.branch_proj = nn.SequentialCell(branch_proj)
        else:
            self.branch_proj = None
        # fixme: The operator in MindSpore is a class. Instantiate the operator in the `__init__` function
        #  and then use it in the `construct` function, which is beneficial for acceleration.
        self.concat = ops.Concat(1)
        self.transpose = ops.Transpose()
        self.reshape = ops.Reshape()

    def construct(self, old_x):
        if self.stride == 1:
            x_proj, x = self.channel_shuffle(old_x)
            return self.concat((x_proj, self.branch_main(x)))
        if self.stride == 2:
            x_proj = old_x
            x = old_x
            return self.concat((self.branch_proj(x_proj), self.branch_main(x)))
        return None

    def channel_shuffle(self, x):
        batch_size, num_channels, height, width = x.shape
        x = self.reshape(x, (batch_size * num_channels // 2, 2, height * width,))
        x = self.transpose(x, (1, 0, 2,))
        x = self.reshape(x, (2, -1, num_channels // 2, height, width,))
        # fixme: Why do we have to index x[0] by x[0:1] and then squeezing it?
        return x[0], x[1]


class ShuffleNetV2(nn.Cell):
    r"""ShuffleNetV2 model class, based on
    `"ShuffleNet V2: Practical Guidelines for Efficient CNN Architecture Design" <https://arxiv.org/abs/1807.11164>`_

    Args:
        n_class (int) : number of classification classes.
        model_size (str) : scale factor which controls the number of channels.
    """

    def __init__(self, n_class=1000, model_size='1.5x'):
        super(ShuffleNetV2, self).__init__()
        print('model size is ', model_size)

        self.stage_repeats = [4, 8, 4]
        self.model_size = model_size
        if model_size == '0.5x':
            self.stage_out_channels = [-1, 24, 48, 96, 192, 1024]
        elif model_size == '1.0x':
            self.stage_out_channels = [-1, 24, 116, 232, 464, 1024]
        elif model_size == '1.5x':
            self.stage_out_channels = [-1, 24, 176, 352, 704, 1024]
        elif model_size == '2.0x':
            self.stage_out_channels = [-1, 24, 244, 488, 976, 2048]
        else:
            raise NotImplementedError

        # building first layer
        input_channel = self.stage_out_channels[1]
        self.first_conv = nn.SequentialCell([
            nn.Conv2d(3, input_channel, 3, 2,
                      pad_mode='pad', padding=1, has_bias=False),
            nn.BatchNorm2d(input_channel),
            nn.ReLU(),
        ])
        self.max_pool = nn.MaxPool2d(kernel_size=3, stride=2, pad_mode='same')

        self.features = []
        for idxstage in range(len(self.stage_repeats)):
            numrepeat = self.stage_repeats[idxstage]
            output_channel = self.stage_out_channels[idxstage + 2]

            for i in range(numrepeat):
                if i == 0:
                    self.features.append(ShuffleV2Block(input_channel, output_channel,
                                                        mid_channels=output_channel // 2, ksize=3, stride=2))
                else:
                    self.features.append(ShuffleV2Block(input_channel // 2, output_channel,
                                                        mid_channels=output_channel // 2, ksize=3, stride=1))
                input_channel = output_channel

        self.features = nn.SequentialCell(self.features)

        self.conv_last = nn.SequentialCell([
            nn.Conv2d(input_channel, self.stage_out_channels[-1], 1, 1,
                      pad_mode='pad', padding=0, has_bias=False),
            nn.BatchNorm2d(self.stage_out_channels[-1]),
            nn.ReLU()
        ])
        self.global_pool = nn.AvgPool2d(7)
        if self.model_size == '2.0x':
            self.dropout = nn.Dropout(keep_prob=0.8)
        self.classifier = nn.Dense(self.stage_out_channels[-1], n_class, has_bias=False)
        self._initialize_weights()
        self.reshape = ops.Reshape()

    def construct(self, x):
        x = self.first_conv(x)
        x = self.max_pool(x)
        x = self.features(x)
        x = self.conv_last(x)

        x = self.global_pool(x)
        if self.model_size == '2.0x':
            x = self.dropout(x)
        x = self.reshape(x, (-1, self.stage_out_channels[-1]))
        x = self.classifier(x)
        return x

    def _initialize_weights(self):
        for name, m in self.cells_and_names():
            if isinstance(m, nn.Conv2d):
                if 'first' in name:
                    m.weight.set_data(init.initializer(init.Normal(0.01, 0), m.weight.shape))
                else:
                    m.weight.set_data(init.initializer(init.Normal(1.0 / m.weight.shape[1], 0), m.weight.shape))
                if m.bias is not None:
                    m.bias.set_data(init.initializer(init.Constant(0), m.bias.shape))
            elif isinstance(m, nn.BatchNorm2d) or isinstance(m, nn.BatchNorm1d):
                m.gamma.set_data(init.initializer(init.Constant(1), m.gamma.shape))
                if m.beta is not None:
                    m.beta.set_data(init.initializer(init.Constant(0.0001), m.beta.shape))
                m.moving_mean.set_data(init.initializer(init.Constant(0), m.moving_mean.shape))
            elif isinstance(m, nn.Dense):
                m.weight.set_data(init.initializer(init.Normal(0.01, 0), m.weight.shape))
                if m.bias is not None:
                    m.bias.set_data(init.initializer(init.Constant(0), m.bias.shape))


def shufflenet_v2_x0_5(**kwargs):
    return ShuffleNetV2(model_size='0.5x', **kwargs)


def shufflenet_v2_x1_0(**kwargs):
    return ShuffleNetV2(model_size='1.0x', **kwargs)


def shufflenet_v2_x1_5(**kwargs):
    return ShuffleNetV2(model_size='1.5x', **kwargs)


def shufflenet_v2_x2_0(**kwargs):
    return ShuffleNetV2(model_size='2.0x', **kwargs)


if __name__ == '__main__':
    import numpy as np
    import mindspore
    from mindspore import Tensor

    model = shufflenet_v2_x1_0()
    print(model)
    dummy_input = Tensor(np.random.rand(8, 3, 224, 224), dtype=mindspore.float32)
    y = model(dummy_input)
    print(y.shape)
