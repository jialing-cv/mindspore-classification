# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""scheduler factory"""

from .warmup_cosine_decay_lr import WarmupCosineDecayLR


def create_scheduler(args):
    if args.scheduler == 'warmup_cosine_decay_lr':
        lr_scheduler = WarmupCosineDecayLR(min_lr=args.min_lr,
                                           max_lr=args.max_lr,
                                           warmup_epochs=args.warmup_epochs,
                                           decay_epochs=args.decay_epochs,
                                           steps_per_epoch=args.steps_per_epoch
                                           )
    elif args.scheduler == 'const':
        lr_scheduler = args.lr

    return lr_scheduler
